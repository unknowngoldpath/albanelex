

# AlbañilesDigitales

## Descripción

Albañiles digitales demo pública basada en NX con CI/CD y despliegue a nube de Amazon y NX CLOUD

## Comenzando

### ¿Que podemos vamos a aprender a hacer?

1. Prepara tu entorno, abre un terminal y ejecuta:
```bash
npm ci
```

2. Ejecuta los programas disponibles (todo añadir otras demos):

```bash
### ejecuta estos comandos en una terminal
nx build das-bañiles
nx serve das-bañiles
### la consola mostrará algo así si todo va bien
> nx run das-bañiles:build:production

Entrypoint main 138 KiB = runtime.c1411f2a2fb6c6b8.esm.js 1.03 KiB main.20fb459ca96ea6b4.esm.js 137 KiB
Entrypoint polyfills 90.5 KiB = runtime.c1411f2a2fb6c6b8.esm.js 1.03 KiB polyfills.0b623cd23be80bb2.esm.js 89.4 KiB
Entrypoint styles 1.03 KiB = runtime.c1411f2a2fb6c6b8.esm.js 1.03 KiB styles.ef46db3751d8e999.css 0 bytes
chunk (runtime: runtime) main.20fb459ca96ea6b4.esm.js (main) 144 KiB [initial] [rendered]
chunk (runtime: runtime) polyfills.0b623cd23be80bb2.esm.js (polyfills) 292 KiB [initial] [rendered]
chunk (runtime: runtime) styles.ef46db3751d8e999.css (styles) 50 bytes (javascript) 80 bytes (css/mini-extract) [initial] [rendered]
chunk (runtime: runtime) runtime.c1411f2a2fb6c6b8.esm.js (runtime) 3.01 KiB [entry] [rendered]
webpack compiled successfully (d88e0dc999de2ae8)

 —————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

 >  NX   Successfully ran target build for project das-bañiles (3s)
 
➜  albañiles-digitales git:(main) nx serve das-bañiles       

> nx run das-bañiles:serve:development

<i> [webpack-dev-server] [HPM] Proxy created: /api  -> http://localhost:3333
<i> [webpack-dev-server] Project is running at:
<i> [webpack-dev-server] Loopback: http://localhost:4200/, http://127.0.0.1:4200/
<i> [webpack-dev-server] 404s will fallback to '/index.html'

>  NX  Web Development Server is listening at http://localhost:4200/

Entrypoint main [big] 1.54 MiB (1.83 MiB) = runtime.js 50.8 KiB vendor.js 1.48 MiB main.js 6.55 KiB 3 auxiliary assets
Entrypoint polyfills [big] 756 KiB (918 KiB) = runtime.js 50.8 KiB polyfills.js 705 KiB 2 auxiliary assets
Entrypoint styles [big] 430 KiB (533 KiB) = runtime.js 50.8 KiB styles.css 399 bytes styles.js 378 KiB 2 auxiliary assets
chunk (runtime: runtime) main.js (main) 4.15 KiB [initial] [rendered]
chunk (runtime: runtime) polyfills.js (polyfills) 658 KiB [initial] [rendered]
chunk (runtime: runtime) runtime.js (runtime) 34.1 KiB [entry] [rendered]
chunk (runtime: runtime) styles.css, styles.js (styles) 361 KiB (javascript) 398 bytes (css/mini-extract) [initial] [rendered]
chunk (runtime: runtime) vendor.js (vendor) (id hint: vendor) 1.47 MiB [initial] [rendered] split chunk (cache group: vendor) (name: vendor)
webpack compiled successfully (d33893282996c56f)
No issues found.
```

3. Visita los enlaces que aparecen en los logs de la consola y comprueba que la aplicación se ha cargado bien.

## Lista de proyectos y 

- das-bañiles : Un proyecto de aplicación web basado en react con una api en express
- albanelev: Un proyecto de blog basando en eleventy
- Add... extra docs from nx.dev
### Templates a añadir basados en los plugins existentes

- El objetivo de estos plugins es facilitarnos el desarrollo de modulos para el temario existente aplicando herramientas y tecnología existente.

>  NX   Installed plugins:

   - [x] nx (executors)
   - [x] @nrwl/cypress (executors,generators)
   - [x] @nrwl/express (generators)
   - [x] @nrwl/jest (executors,generators)
   - [x] @nrwl/js (executors,generators)
   - [x] @nrwl/linter (executors,generators)
   - [x] @nrwl/node (executors,generators)
   - [x] @nrwl/react (executors,generators)
   - [x] @nrwl/storybook (executors,generators)
   - [x] @nrwl/web (executors,generators)
   - [x] @nrwl/workspace (executors,generators)
   - [x] @nrwl/angular (generators)
   - [x] @nrwl/nest (executors,generators)
   - [x] @nrwl/next (executors,generators)
   - [x] @nrwl/nx-plugin (executors,generators)

### Plugins Extras

- Plugins para desarrollar otros tipos de sofware que podemos sugerir a los alumnos para que investiguen además de lo básico con los instalados.

   - [ ] nx-electron - An Nx plugin for developing Electron applications
   - [ ] ngx-deploy-npm - Publish your libraries to NPM with just one command.
   - [ ] @nxtend/ionic-react - An Nx plugin for developing Ionic React applications and libraries
   - [ ] @nxtend/ionic-angular - An Nx plugin for developing Ionic Angular applications and libraries
   - [ ] @nxtend/capacitor - An Nx plugin for developing cross-platform applications using Capacitor
   - [ ] @nxtend/firebase - An Nx plugin for developing applications using Firebase
   - [ ] @nx-tools/nx-docker - Nx plugin to build docker images of your affected apps
   - [ ] @nxext/svelte - Nx plugin to use Svelte within nx workspaces
   - [ ] @nxext/stencil - Nx plugin to use StencilJs within nx workspaces
   - [ ] @nxext/vite - Nx plugin to use ViteJS within nx workspaces
   - [ ] @nxext/solid - Nx plugin to use SolidJS within nx workspaces
   - [ ] @nx-go/nx-go - Nx plugin to use Go in a Nx workspace
   - [ ] @nxrocks/nx-flutter - Nx Plugin adding first class support for Flutter in your Nx workspace
   
- Arquitectura
   @nx-clean/plugin-core - Nx Plugin to generate projects following Clean Architecture practices

- SpringBoot 
   @jnxplus/nx-boot-gradle - Nx plugin to add Spring Boot and Gradle multi-project builds support to Nx workspace
   @jnxplus/nx-boot-maven - Nx plugin to add Spring Boot and Maven multi-module project support to Nx workspace

- Constructor de sitios estáticos
   @nxtensions/astro - Nx plugin adding first class support for Astro (https://astro.build).

- Sistemas
   @nxrs/cargo - Nx plugin adding first-class support for Rust applications and libraries.

## Herramientas y otros (EN)

This project was generated using [Nx](https://nx.dev).

<p style="text-align: center;"><img src="https://raw.githubusercontent.com/nrwl/nx/master/images/nx-logo.png" width="450"></p>

🔎 **Smart, Fast and Extensible Build System**

## Adding capabilities to your workspace

Nx supports many plugins which add capabilities for developing different types of applications and different tools.

These capabilities include generating applications, libraries, etc as well as the devtools to test, and build projects as well.

Below are our core plugins:

- [React](https://reactjs.org)
  - `npm install --save-dev @nrwl/react`
- Web (no framework frontends)
  - `npm install --save-dev @nrwl/web`
- [Angular](https://angular.io)
  - `npm install --save-dev @nrwl/angular`
- [Nest](https://nestjs.com)
  - `npm install --save-dev @nrwl/nest`
- [Express](https://expressjs.com)
  - `npm install --save-dev @nrwl/express`
- [Node](https://nodejs.org)
  - `npm install --save-dev @nrwl/node`

There are also many [community plugins](https://nx.dev/community) you could add.

## Generate an application

Run `nx g @nrwl/react:app my-app` to generate an application.

> You can use any of the plugins above to generate applications as well.

When using Nx, you can create multiple applications and libraries in the same workspace.

## Generate a library

Run `nx g @nrwl/react:lib my-lib` to generate a library.

> You can also use any of the plugins above to generate libraries as well.

Libraries are shareable across libraries and applications. They can be imported from `@albañiles-digitales/mylib`.

## Development server

Run `nx serve my-app` for a dev server. Navigate to http://localhost:4200/. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `nx g @nrwl/react:component my-component --project=my-app` to generate a new component.

## Build

Run `nx build my-app` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `nx test my-app` to execute the unit tests via [Jest](https://jestjs.io).

Run `nx affected:test` to execute the unit tests affected by a change.

## Running end-to-end tests

Run `nx e2e my-app` to execute the end-to-end tests via [Cypress](https://www.cypress.io).

Run `nx affected:e2e` to execute the end-to-end tests affected by a change.

## Understand your workspace

Run `nx graph` to see a diagram of the dependencies of your projects.

## Further help

Visit the [Nx Documentation](https://nx.dev) to learn more.



## ☁ Nx Cloud

### Distributed Computation Caching & Distributed Task Execution

<p style="text-align: center;"><img src="https://raw.githubusercontent.com/nrwl/nx/master/images/nx-cloud-card.png"></p>

Nx Cloud pairs with Nx in order to enable you to build and test code more rapidly, by up to 10 times. Even teams that are new to Nx can connect to Nx Cloud and start saving time instantly.

Teams using Nx gain the advantage of building full-stack applications with their preferred framework alongside Nx’s advanced code generation and project dependency graph, plus a unified experience for both frontend and backend developers.

Visit [Nx Cloud](https://nx.app/) to learn more.
