import React, { useEffect, useState } from 'react';
import { Message } from '@albañiles-digitales/api-interfaces';
import { NxWelcome } from "./nx-welcome";

export const App = () => {
  const [m, setMessage] = useState<Message>({ message: '' });

  useEffect(() => {
    fetch('/api')
      .then((r) => r.json())
      .then(setMessage);
  }, []);

  return (
    <>
      <div style={{ textAlign: 'center' }}>
        <h1>Welcome to das-bañiles!</h1>
        <img
          width="450"
          src="https://raw.githubusercontent.com/nrwl/nx/master/images/nx-logo.png"
          alt="Nx - Smart, Fast and Extensible Build System"
        />
      </div>
      <div>{m.message} and say hello to NX</div>
      {NxWelcome({title:'Functional Programmer'})}
    </>
  );
};

export default App;
